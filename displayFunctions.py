# Functions to be used (possibly temporarily) to display quotes
from bottle import template, SimpleTemplate

#Prints a list of quotes
def print_quote_list(qList):
	qStr = ""
	for q in qList['results']:
		qStr += template('./static/quote_view.html', quote = q)
	return qStr

# Prints a quote and its data given a quote from array
def print_quote(qData):
	likes = str(qData['likes'])
	dislikes = str(qData['dislikes'])
	return "Quotes: " + qData['quote'] + "<br />Likes:" + likes + "<br />Dislikes:" + dislikes + "<br /><br />"