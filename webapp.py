#Authors: Ben Walters, Hugo A. Lopez & Ryan Thomas
#Version 1.0
from bottle import get, post, request, static_file, route, run, error, redirect, response, template, SimpleTemplate
import json, http.client, urllib
from functions import *
from UserFunctions import *
from QuoteFunctions import *

#variable to append 'ho home' function (temporary, until client side gets a purdy' website going)
global goHome 
goHome = '<br /><a href="/">Go Home</a>'

@route('/')
def page_index():
	return static_file('index.html', root='./static/')

@route('/login')
def page_login():
	return static_file('login.html', root='./static/')
	
@route('/register')
def page_register():
	return static_file('register.html', root='./static/')
	
@route('/quote')
def page_quotes():
	return static_file('quote_add.html', root='./static/')
	
@route('/login-user', method='POST')
def login_user():
	userName = request.forms.get('name')
	userPassword = request.forms.get('password')
	if(request.get_cookie("username")):
		return "You're already logged in!" + goHome	
	account = get_account_password(userName, userPassword)
	if account['results']:
		response.set_cookie("username", userName)
		redirect('/')
	else:
		return "user/password match not found" + goHome

@route('/register-user', method='POST')
def register_user():
	userName = request.forms.get('name')
	userPassword = request.forms.get('password')
	password_confirm = request.forms.get('password_confirm')
	
	if(userName == "" or userPassword == ""):
		return "neither password nor username can be blank!" + goHome
		
	if(userPassword != password_confirm):
		return "passwords do not match" + goHome
		
	account = get_account(userName) #account is array with only one element. 
	if (account['results']):	
		return "account already exists!" + goHome
	else:
		create_account(userName, userPassword)
		accountInfo = "account name: " + userName + "<br />account password: " + userPassword
		response.set_cookie("username", userName)
		return "account created succesfully, you are logged in<br />" + accountInfo + goHome

@route('/post-quote', method = 'POST')
def post_quote():
	quote = request.forms.get('quote')
	if not request.get_cookie("username"):
		return create_quote("anonymous", quote)
	create_quote(request.get_cookie("username"), quote)
	return "Quote created!" + goHome
		
@route('/quote/<qID>')
def get_quote_byID(qID):
	quote = get_quote(qID)
	if(quote['error']):
		return "Quote doesn't exist :(" + goHome
	return template('static/quote_view.html', quote = quote)

	
@route('/top')
def top_quotes():
	qData = get_sorted_quotes("-likes",10)
	quoteList = qData['results']
	return template('templates/quote_view.tpl', quotes = quoteList, title = "Top Submissions") + goHome
	
@route('/all')
def display_quotes():
	redirect('/all/1')

@route('/all/<page>')
def display_quote_page(page):
	items = 10
	pnum = int(page)
	params = urllib.parse.urlencode({"sort":"createdAt","count":1,"limit":items,"skip":items*(pnum-1)})
	quote = get_quotes_params(params)
	size = quote['count']
	qData = template('templates/quote_view.tpl', quotes = quote['results'], title = "All quotes (page " + page + ")")
	numPages =  num_pages(size, items)
	if (int(page) > 1):
		qData += "<a href=\"/all/" + str(int(page)-1) + "\">" + str("<< Previous") + "</a>&nbsp"
	for p in range(1, numPages + 1):
		qData += "<a href=\"/all/" + str(p) + "\">" + str(p) + "</a>&nbsp"
	if (int(page) < numPages):
		qData += "<a href=\"/all/" + str(int(page)+1) + "\">" + str("Next >>") + "</a>&nbsp"
	return qData
	
@route('/user/quotes')
def get_my_quotes():
	user = request.get_cookie("username")
	if not request.get_cookie("username"):
		return "You aren't logged in!" + goHome
		
	quote = get_user_quotes(user)
	quoteList = quote['results']
	return template('templates/quote_view.tpl', quotes = quoteList, title = "My Quotes", deletable = True) + goHome
	
@route('/logout')
def log_out():
	if not request.get_cookie("username"):
		return "You have to login before you can log out, you know." + goHome
	response.delete_cookie("username")
	return "You've been logged out" + goHome
	
@route('/user')
def get_user():
	if not request.get_cookie("username"):
		return "Hey, you aren't logged in!" + goHome
	acc = get_account(request.get_cookie("username"))
	acc = acc['results']
	user = acc[0]
	accountInfo = "User: " + user['name'] + "<br />Password: " + user['password'] + "<br />registered @ " + user['createdAt']
	accountInfo += "<br />Edit profile: " + '<a href="/user/edit">edit</a>'
	accountInfo += '<br /><a href="/user/quotes">My quotes</a>'
	return accountInfo
#we didn't need the if, 

@route('/verify-edit', method = 'POST')
def verify_edit():
	userAccount = request.get_cookie("username")
	password_old = request.forms.get('password_old')
	password_new = request.forms.get('password_new')
	password_confirm = request.forms.get('password_confirm')
		
	if(password_new == "" or password_old == ""):
		return "password can't be blank!" + goHome
		
	if(password_new != password_confirm):
		return "passwords do not match" + goHome
		
	account = get_account_password(userAccount, password_old)
	if not account['results']:
		return "Your password is incorrect" + goHome
		
	update_account_password(userAccount, password_new)
	return "You have succesfully changed your password" + goHome

@route('/user/edit')
def edit_info():
	if not request.get_cookie("username"):
		return "Hey, you aren't logged in!" + goHome
	print("Redirected!")
	return static_file('edit.html', root='./static/') 


'''Left this here, we need a reference for the template thing, we're gonna have to do it for 
a few things, save a bit of work if we remember how to do this stuff.
@route('/users')
def users():
	accounts = get_accounts()
	return template('users.tpl', accounts = accounts['results'])
'''
@route('/random')
def random():
	#return template('static/quote_view.html', quote = quote)
	quotes = get_random_quote()
	q = quotes['results']
	return template('templates/quote_view.tpl', quotes = q, title = "Random Quote") + goHome
	
@route('/like', method='POST')
def like_quote():
	quoteID = str(request.forms.get('qID'))
	update_quote(quoteID, True)
	return "quote liked!" + '<br /><a href="/random">Another Random Quote</a>' + goHome
	
@route('/dislike', method='POST')
def dislike_quote():
	quoteID = str(request.forms.get('qID'))
	update_quote(quoteID, False)
	return "quote disliked. :(" + '<br /><a href="/random">Another Random Quote</a>' + goHome

@route('/delete_form', method='POST')
def delete_quote_form():
	qID = request.forms.get('qID')
	response = delete_quote(qID)
	if (response): # if there is an error - it is not empty
		return "Quote could not be deleted" + goHome
	else:
		return "Quote deleted" + goHome

# WILL BE USED LATER
@route('/delete_form2', method='POST')
def delete_quote_form2():
	qID = request.forms.get('qID')
	username = request.get_cookie("username")
	return template('templates/delete_form', qID = qID, username = username) + goHome

## NOT USED RIGHT NOW
##@route('/delete', method='POST')
def UNUSED(): ##delete_quote():
	qID = request.forms.get('qID')
	userName = request.forms.get('name')
	userPassword = request.forms.get('password')
	response = delete_quote(qID, userName, userPassword)
	if account['results']:
		response.set_cookie("username", userName)
		redirect('/')
	else:
		return "user/password match not found" + goHome


run(host='localhost', port=8080, debug=True, reloader=True)